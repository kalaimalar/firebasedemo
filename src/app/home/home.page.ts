import { Component, OnInit } from '@angular/core';
// import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';

import * as firebase from "firebase"

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  // recaptchaVerifier;
  // confirmationResult: firebase.auth.ConfirmationResult;
  // otpSent: any = false;
  // phoneNumber;


  // verificationId: any;
  code = '';
  showCodeInput = false;
  mobileNumber;

  constructor(public af: AngularFireAuth,

    // private storage: Storage, 
    private afs: AngularFirestore, 
    private afAuth: AngularFireAuth
    // public recaptchaVerifier: firebase.auth.RecaptchaVerifier
  ) { }

  ngOnInit() {
    // this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', { 'size': 'invisible' });
  }
  // ionViewDidLoad() {
  //   this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
  // }

  // sendOtp() {
  //   let phNumber = (<HTMLInputElement>document.getElementById("phoneNumber")).value;
  //   this.af.signInWithPhoneNumber(phNumber, this.recaptchaVerifier).then((result) => {
  //     this.otpSent = true;
  //     this.phoneNumber = phNumber;
  //     this.confirmationResult = result;
  //   }).catch(err => {
  //     alert(err);
  //   })
  // }
  // verifyOtp() {
  //   var otp = (<HTMLInputElement>document.getElementById("otp")).value;
  //   this.confirmationResult.confirm(otp).then(() => {
  //     alert('OTP Verified');
  //   }).catch(err => {
  //     alert('Err');
  //   }) 
  // }


  sendOtp(){
    console.log(this.mobileNumber);
    this.afAuth.signInWithPhoneNumber('+' + 91 + this.mobileNumber, new firebase.auth.RecaptchaVerifier
    (
      're-container', {
        size: 'invisible'
      }))
      .then((creds) => {  
        alert(creds);
      })
      .catch(err => alert(err));
  }
  onOtpSend(){

  }
  verify(){

  }
}
